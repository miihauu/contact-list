# Contact list

### You can check it here: https://contacts-list-task.netlify.app/

## Recruitment task

1. Getting data from external server
2. Displaying contacts
3. Marking contacts and console their id's

# Usage

1. Clone the repository

``` git clone https://gitlab.com/miihauu/contact-list.git ```

2. Install dependencies

``` yarn install ``` 
or
``` npm install ```

3. Run

``` yarn start ```
 or
``` npm start ```
