/* eslint-disable no-undef */
import React, { useEffect, useState } from 'react';
import { connect, useDispatch } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { number, arrayOf, shape, func } from 'prop-types';
import { contactProps } from './utils/sharedProps';
import Contact from './components/Contact';
import SearchContact from './components/SearchContact';
import Title from './components/Title';
import { selectContactAction } from './redux/actionCreators';
import { filterByName } from './utils/helper';
import { FETCH_CONTACTS, FETCH_CONTACTS_FAILED } from './redux/types';

const Contacts = ({ contactsList, selectContact, selectedContacts }) => {
  const [filter, setFilter] = useState('');
  const dispatch = useDispatch();

  useEffect(() => {
    fetch(`https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json`)
      .then(response => response.json())
      .then(data => dispatch({ type: FETCH_CONTACTS, payload: data }))
      .catch(error => dispatch({ type: FETCH_CONTACTS_FAILED, payload: error }));
  }, []);

  const handleName = e => setFilter(e.target.value);
  const handleContact = id => selectContact(id);

  // Console.log added as a task requirement
  console.log('selected contacts ids:', selectedContacts);

  return (
    <div className="contacts">
      <Title />
      <SearchContact onChange={handleName} />
      <div className="contacts__content">
        {!contactsList.length && <CircularProgress className="contacts__loader" />}
        {filterByName(contactsList, filter).map(contact => (
          <Contact
            key={`${contact.firstName}${contact.lastName}${contact.id}`}
            contact={contact}
            handleClick={handleContact}
            selected={selectedContacts.includes(contact.id)}
          />
        ))}
      </div>
    </div>
  );
};

const mapStateToProps = ({ contacts }) => ({
  selectedContacts: contacts.selected,
  contactsList: contacts.list
});

const mapDispatchToProps = dispatch => ({
  selectContact: payload => dispatch(selectContactAction(payload))
});

Contacts.propTypes = {
  contactsList: arrayOf(shape(contactProps)).isRequired,
  selectContact: func.isRequired,
  selectedContacts: arrayOf(number).isRequired
};

export const ContactsComponent = Contacts;
export default connect(mapStateToProps, mapDispatchToProps)(Contacts);
