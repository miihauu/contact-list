/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '../../components/Avatar';

const setup = ({ selected = false, avatar = '', firstName = 'John', lastName = 'Doe' }) =>
  shallow(<Avatar selected={selected} avatar={avatar} firstName={firstName} lastName={lastName} />);

describe('Avatar component', () => {
  describe('when avatar is available', () => {
    const avatarComponent = setup({ avatar: 'any.link.jpg' });

    it('renders image with correct class name', () => {
      expect(avatarComponent.find('img').prop('className')).toMatch('contact__avatar');
    });
  });
  describe('when contact is selected', () => {
    const checkbox = setup({ selected: true }).find(Checkbox);

    it('renders checkbox', () => {
      expect(checkbox.prop('className')).toMatch('contact__avatar');
      expect(checkbox.prop('checked')).toEqual(true);
      expect(checkbox).toHaveLength(1);
    });
  });
  describe('when avatar is not provided', () => {
    const avatar = setup({ avatar: '' });

    it('returns initials', () => {
      expect(avatar.find('.contact__avatar').text()).toEqual('JD');
    });
  });
});
