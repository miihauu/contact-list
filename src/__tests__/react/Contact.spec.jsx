/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';
import Contact from '../../components/Contact';

const contact = {
  first_name: 'John',
  last_name: 'Doe',
  avatar: 'link',
  id: 1,
  email: 'johndoe@gmail.com'
};

const setup = ({ selected = false, handleClick = jest.fn() }) =>
  shallow(<Contact handleClick={handleClick} selected={selected} contact={contact} />);

describe('Contact component', () => {
  const contactComponent = setup({});
  const firstDivClassName = component => component.find('div').first().prop('className');

  it('renders contact component', () => {
    expect(firstDivClassName(contactComponent)).toEqual('contact');
  });

  it('renders personal details', () => {
    expect(contactComponent.find('.personal-data__full-name').text()).toEqual('John Doe');
    expect(contactComponent.find('.personal-data__email').text()).toEqual('johndoe@gmail.com');
  });

  it('returns additional class, when contact was selected', () => {
    const contactSelected = setup({ selected: true });

    expect(firstDivClassName(contactSelected)).toEqual('contact contact--selected');
  });
});
