/* eslint-disable no-undef */
import React from 'react';
import { mount } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { ContactsComponent } from '../../Contacts';
import Title from '../../components/Title';
import Contact from '../../components/Contact';
import SearchContact from '../../components/SearchContact';

const store = {
  contacts: {
    list: [
      { first_name: 'John', last_name: 'Doe', email: 'johndoe@gmail.com', avatar: 'link.jpg', id: 1 },
      { first_name: 'Martin', last_name: 'Schmitt', email: 'martinschmitt@gmail.com', avatar: 'link.jpg', id: 2 }
    ],
    selected: [2],
    error: []
  }
};

const mockStore = configureMockStore([]);
const mockOnClick = jest.fn();

const setup = ({ contactsList = store.contacts.list }) =>
  mount(
    <Provider store={mockStore(store)}>
      <ContactsComponent
        selectContact={mockOnClick}
        contactsList={contactsList}
        selectedContacts={store.contacts.selected}
      />
    </Provider>
  );

describe('Contacts', () => {
  const contacts = setup({});

  it('renders contacts component', () => {
    expect(contacts.find('div').first().prop('className')).toEqual('contacts');
    expect(contacts.find(Title)).toHaveLength(1);
    expect(contacts.find(SearchContact)).toHaveLength(1);
  });

  it('renders loader, when there is no contacts yet', () => {
    const loader = setup({ contactsList: [] }).find(CircularProgress);

    expect(loader).toHaveLength(1);
  });

  it('renders two contact components', () => {
    expect(contacts.find(Contact)).toHaveLength(2);
  });

  it('clicks on first contact component and returns its id', () => {
    const firstContactId = store.contacts.list[0].id;
    contacts.find(Contact).first().simulate('click', firstContactId);
    expect(mockOnClick.mock.calls[0][0]).toBe(firstContactId);
  });
});
