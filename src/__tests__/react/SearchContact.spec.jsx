/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';
import { TextField } from '@material-ui/core';
import SearchContact from '../../components/SearchContact';

const mockChange = jest.fn();
const searchComponent = shallow(<SearchContact onChange={mockChange} />);

describe('SearchContact', () => {
  it('renders text field component', () => {
    expect(searchComponent.find(TextField)).toHaveLength(1);
  });

  it('changes value of filter in search input', () => {
    const filter = 'adam';
    searchComponent.find(TextField).simulate('change', filter);
    expect(mockChange.mock.calls[0][0]).toBe(filter);
  });
});
