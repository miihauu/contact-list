/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';
import Title from '../../components/Title';

const header = shallow(<Title />);

describe('Header', () => {
  it('correctly renders component', () => {
    expect(header.find('div').prop('className')).toEqual('contacts__title');
    expect(header.find('.contacts__label').text()).toEqual('Contacts');
    expect(header.find('PermContactCalendarIcon').prop('className')).toEqual('contacts__title-icon');
  });
});
