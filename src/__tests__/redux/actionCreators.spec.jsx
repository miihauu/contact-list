/* eslint-disable no-undef */
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as types from '../../redux/types';
import { selectContactAction } from '../../redux/actionCreators';

const mockStore = configureMockStore([thunk]);

describe('selectContactAction', () => {
  const store = mockStore({
    contacts: {
      list: [
        { first_name: 'John', last_name: 'Doe', email: 'johndoe@gmail.com', avatar: 'link.jpg', id: 1 },
        { first_name: 'Martin', last_name: 'Schmitt', email: 'martinschmitt@gmail.com', avatar: 'link.jpg', id: 2 }
      ],
      selected: [3],
      error: []
    }
  });

  it('creates an action to add new contact id to store', () => {
    const actions = store.getActions();
    store.dispatch(selectContactAction(1));

    expect(actions).toContainEqual({ type: types.SELECT_CONTACT, payload: 1 });
  });
  it('creates an action to removes existing contact id', () => {
    const actions = store.getActions();
    store.dispatch(selectContactAction(3));

    expect(actions).toContainEqual({ type: types.DELETE_SELECTION, payload: 3 });
  });
});
