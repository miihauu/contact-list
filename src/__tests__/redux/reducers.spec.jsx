/* eslint-disable no-undef */
import * as types from '../../redux/types';
import contacts, { contactsInitialState } from '../../redux/reducers';

describe('contacts reducers', () => {
  describe('FETCH_CONTACTS', () => {
    const payload = [
      { first_name: 'John', last_name: 'Doe', email: 'johndoe@gmail.com', avatar: 'link.jpg', id: 1 },
      { first_name: 'Martin', last_name: 'Schmitt', email: 'martinschmitt@gmail.com', avatar: 'link.jpg', id: 2 }
    ];

    it('returns list of contacts', () => {
      expect(contacts(contactsInitialState, { type: types.FETCH_CONTACTS, payload })).toEqual({
        ...contactsInitialState,
        list: payload
      });
    });
  });

  describe('FETCH_CONTACTS_FAILED', () => {
    it('returns errors, when appeared', () => {
      expect(contacts(contactsInitialState, { type: types.FETCH_CONTACTS_FAILED, payload: 'Error' })).toEqual({
        ...contactsInitialState,
        error: 'Error'
      });
    });
  });

  describe('SELECT_CONTACT', () => {
    it('returns array with selected contact ids', () => {
      expect(contacts(contactsInitialState, { type: types.SELECT_CONTACT, payload: 1 })).toEqual({
        ...contactsInitialState,
        selected: [1]
      });
    });
  });

  describe('DELETE_SELECTION', () => {
    const initialStore = { ...contactsInitialState, selected: [1, 2, 3] };
    const expectedStore = { ...contactsInitialState, selected: [1, 3] };

    it('removes given id from "selected" array', () => {
      expect(contacts(initialStore, { type: types.DELETE_SELECTION, payload: 2 })).toEqual(expectedStore);
    });
  });
});
