/* eslint-disable no-undef */
import { filterByName } from '../../utils/helper';

describe('filterByName', () => {
  const names = [
    { first_name: 'Adam', last_name: 'Johnson' },
    { first_name: 'Stephan', last_name: 'Wright' },
    { first_name: 'Garry', last_name: 'Lineker' },
    { first_name: 'George', last_name: 'Russell' }
  ];
  it('filters name, when user write small letter as a first', () => {
    expect(filterByName(names, 'wri')).toEqual([{ first_name: 'Stephan', last_name: 'Wright' }]);
    expect(filterByName(names, 'step')).toEqual([{ first_name: 'Stephan', last_name: 'Wright' }]);
  });

  it('filters name, when user write capital letter as a first', () => {
    expect(filterByName(names, 'Ge')).toEqual([{ first_name: 'George', last_name: 'Russell' }]);
    expect(filterByName(names, 'Ru')).toEqual([{ first_name: 'George', last_name: 'Russell' }]);
  });

  it('returns object, when given string has found inside name', () => {
    expect(filterByName(names, 'org')).toEqual([{ first_name: 'George', last_name: 'Russell' }]);
    expect(filterByName(names, 'sse')).toEqual([{ first_name: 'George', last_name: 'Russell' }]);
  });
});
