import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { bool, string } from 'prop-types';

const Avatar = ({ selected, avatar, firstName, lastName }) => {
  if (selected) {
    return (
      <Checkbox
        className="contact__avatar"
        checked={selected}
        icon={<RadioButtonUncheckedIcon />}
        checkedIcon={<CheckCircleIcon />}
      />
    );
  }
  if (avatar) return <img src={avatar} alt={`${firstName} ${lastName} avatar`} className="contact__avatar" />;
  return <span className="contact__avatar">{`${firstName[0]}${lastName[0]}`}</span>;
};

Avatar.propTypes = {
  firstName: string.isRequired,
  lastName: string.isRequired,
  avatar: string,
  selected: bool.isRequired
};

Avatar.defaultProps = {
  avatar: ''
};

export default Avatar;
