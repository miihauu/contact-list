/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/interactive-supports-focus */
import React from 'react';
import clsx from 'clsx';
import { bool, shape, func } from 'prop-types';
import { contactProps } from '../utils/sharedProps';
import Avatar from './Avatar';

const Contact = ({
  contact: { first_name: firstName, last_name: lastName, avatar, email, id },
  handleClick,
  selected
}) => {
  return (
    <div className={clsx('contact', selected && 'contact--selected')} onClick={() => handleClick(id)} role="button">
      <Avatar firstName={firstName} lastName={lastName} avatar={avatar} selected={selected} />
      <div className="personal-data">
        <div className="personal-data__full-name">{`${firstName} ${lastName}`}</div>
        <div className="personal-data__email">{email}</div>
      </div>
    </div>
  );
};

Contact.propTypes = {
  selected: bool.isRequired,
  handleClick: func,
  contact: shape(contactProps).isRequired
};

Contact.defaultProps = {
  handleClick: null
};

export default Contact;
