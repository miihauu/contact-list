import React from 'react';
import { func } from 'prop-types';
import { TextField } from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';

const SearchContact = ({ onChange }) => {
  return (
    <TextField
      type="search"
      variant="outlined"
      fullWidth
      onChange={onChange}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        )
      }}
    />
  );
};

SearchContact.propTypes = {
  onChange: func.isRequired
};

export default SearchContact;
