import React from 'react';
import PermContactCalendarIcon from '@material-ui/icons/PermContactCalendar';

const Title = () => {
  return (
    <div className="contacts__title">
      <PermContactCalendarIcon className="contacts__title-icon" />
      <span className="contacts__label">Contacts</span>
    </div>
  );
};

export default Title;
