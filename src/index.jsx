/* eslint-disable no-undef */
import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import Contacts from './Contacts';
import configureStore from './redux/store';
import './stylesheets/main.scss';

ReactDOM.render(
  <Provider store={configureStore()}>
    <Contacts />
  </Provider>,
  document.getElementById('root')
);
