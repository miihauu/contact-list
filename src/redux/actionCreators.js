/* eslint-disable no-undef */
/* eslint-disable import/prefer-default-export */
import { DELETE_SELECTION, SELECT_CONTACT } from './types';

export const selectContactAction = payload => (dispatch, getState) => {
  const {
    contacts: { selected }
  } = getState();
  return dispatch({ type: selected.includes(payload) ? DELETE_SELECTION : SELECT_CONTACT, payload });
};
