import * as types from './types';

export const contactsInitialState = {
  list: [],
  error: [],
  selected: []
};

const contacts = (state = contactsInitialState, { type, payload }) => {
  switch (type) {
    case types.FETCH_CONTACTS: {
      return { ...state, list: payload.sort((a, b) => a.last_name.localeCompare(b.last_name)) };
    }
    case types.FETCH_CONTACTS_FAILED: {
      return { ...state, error: payload };
    }
    case types.SELECT_CONTACT: {
      return { ...state, selected: [...state.selected, payload] };
    }
    case types.DELETE_SELECTION: {
      return { ...state, selected: state.selected.filter(id => id !== payload) };
    }
    default: {
      return state;
    }
  }
};

export default contacts;
