import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import contacts from './reducers';

const configureStore = () => {
  return createStore(
    combineReducers({
      contacts
    }),
    composeWithDevTools(applyMiddleware(thunkMiddleware))
  );
};

export default configureStore;
