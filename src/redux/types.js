export const FETCH_CONTACTS = 'fetch-contacts';
export const FETCH_CONTACTS_FAILED = 'fetch-contacts-failed';
export const SELECT_CONTACT = 'select-contact';
export const DELETE_SELECTION = 'delete-selected';
