/* eslint-disable import/prefer-default-export */
/* eslint-disable camelcase */

export const filterByName = (array, filter) => {
  const matcher = new RegExp(filter, 'i');

  return array.filter(({ first_name, last_name }) => matcher.test(first_name) || matcher.test(last_name));
};
