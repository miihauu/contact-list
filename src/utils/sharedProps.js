/* eslint-disable import/prefer-default-export */

import { number, string } from 'prop-types';

export const contactProps = {
  first_name: string,
  last_name: string,
  avatar: string,
  email: string,
  id: number
};
